from collections import defaultdict, Counter
from datetime import datetime

from helpers.utils import replace_tz, date_converter


class Statistic:
    def __init__(self):
        pass

    def __check_date(self, date: str, date_from: str, date_till: str) -> bool:
        """

        :param date: current date
        :param _from: between from
        :param _till: between till
        :return: bool
        """

        is_check_from = True
        is_check_till = True

        if not date:
            raise Exception('date s empty')

        _date = date_converter(date)

        if date_from:
            _from = date_converter(date_from)
            if _date <= _from:
                is_check_from = False

        if date_till:
            _till = date_converter(date_till)
            if _date >= _till:
                is_check_till = False

        return is_check_from and is_check_till

    def __get_param(self, item: dict, depth: list):
        node = item.copy()
        for param in depth:
            if node.get(param):
                node = node.get(param)
            else:
                return 'IS NULL {}'.format(param)
        return node

    def __counter(self, items, count_depth: list, date_depth: list, date_from, date_till) -> list:
        counter = Counter()

        for item in items:
            count_param = self.__get_param(item, count_depth)
            date = self.__get_param(item, date_depth)
            date = replace_tz(date)

            if self.__check_date(date, date_from, date_till) and count_param:
                counter[count_param] += 1

        return counter.most_common()

    def get_active_commit_users_stat(self, commits: list, date_from: str = None, date_till: str = None) -> list:
        """
        Не у всех узеров есть логин
        https://github.community/t5/GitHub-API-Development-and/Request-for-commits-quot-author-quot-null-and-quot-committer/td-p/35343
        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """

        count_depth = ['author', 'login']
        date_depth = ['commit', 'committer', 'date']
        return self.__counter(commits, count_depth, date_depth, date_from, date_till)
        # counter = Counter()
        # for commit in commits:
        #     commit_date = commit.get('commit').get('committer').get('date')
        #     commit_date = replace_tz(commit_date)
        #
        #     if self.__check_date(commit_date, date_from, date_till):
        #         if commit.get('committer') and commit.get('committer').get('login'):
        #                 login =commit.get('committer').get('login')
        #                 counter[login] += 1
        #
        # return counter.most_common()

    def get_pulls_stat(self, pulls: list, date_from: str = None, date_till: str = None) -> list:
        """

        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """
        count_depth = ['state']
        date_depth = ['created_at']
        return self.__counter(pulls, count_depth, date_depth, date_from, date_till)

    def get_pulls_is_old_stat(self, pulls: list, date_from: str = None, date_till: str = None) -> int:
        """

        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """
        date_now = datetime.now()
        count = 0
        for pull in pulls:
            date = pull.get('created_at')
            date = replace_tz(date)

            if self.__check_date(date, date_from, date_till):
                date = date_converter(date)
                diff = (date_now - date).days
                if diff > 30:
                    count += 1
        return count

    def get_issues_is_old_stat(self, issues: list, date_from: str = None, date_till: str = None) -> int:
        date_now = datetime.now()
        count = 0
        for issue in issues:
            date = issue.get('created_at')
            date = replace_tz(date)

            if self.__check_date(date, date_from, date_till):
                date = date_converter(date)
                diff = (date_now - date).days
                if diff > 14:
                    count += 1
        return count

    def print_active_users(self, items: list, count=30):
        colls = items[:count]
        table_headers = ['login', 'commits count']

        self.print_table(colls, table_headers)

    def print_pulls(self, items: list, count=30):
        colls = items[:count]
        table_headers = ['state', 'count']

        self.print_table(colls, table_headers)

    def print_table(self, colls: list, header: list):
        """

        :param items:
        :param header: columns name
        :return: stdout table
        """

        print('-' * 40)
        print('{:<15s}|{:^17s}'.format(header[0], str(header[1])))
        print('-' * 40)
        for col in colls:
            print('{:<15s}{:^17s}'.format(col[0], str(col[1])))
