import json
from urllib.parse import urljoin, urlencode
from urllib import request
from urllib.error import URLError, HTTPError


class GitHubApi:
    def __init__(self, user, url='https://api.github.com/', token=''):
        self.git_url = url
        self.token = token
        self.user = user

    def __request(self, link: str, params: dict = None):
        url = urljoin(self.git_url, link)
        if params:
            params = urlencode(params)
            url = url + '?' + params
        req = request.Request(url)
        if self.token:
            req.add_header('Authorization', 'token %s' % self.token)
        try:
            with request.urlopen(req) as response:
                data = response.read()
        except HTTPError as e:
            print('Error code: ', e.code)
        except URLError as e:
            print('Reason: ', e.reason)
        try:
            return json.loads(data.decode('utf-8'))
        except ValueError as e:
            print('JSON parse error: ', e)

    def get_commits(self, repo, branch, per_page=100):
        link = 'repos/{}/{}/commits'.format(self.user, repo)

        params = {
            'sha': branch,
            'per_page': per_page
        }
        commint_list = self.__request(link, params)

        if commint_list and len(commint_list) == per_page:
            sha = commint_list[-1].get('sha')
            commits = commint_list[:-1]
            commits.extend(self.get_commits(repo, sha))
        else:
            commits = commint_list

        return commits

    def get_pulls(self, repo, base_branch='master', state='open', per_page=100, page=1):
        link = 'repos/{}/{}/pulls'.format(self.user, repo)

        params = {
            'state': state,
            'per_page': per_page,
            'page': page,
            'base': base_branch
        }
        return self.__get_with_paginate(repo, link, params)

    def get_issues(self, repo, state='open', per_page=100, page=1):
        link = 'repos/{}/{}/issues'.format(self.user, repo)
        params = {
            'state': state,
            'per_page': per_page,
            'page': page,
        }
        return self.__get_with_paginate(repo, link, params)

    def __get_with_paginate(self, repo, link, params):
        list = self.__request(link, params)
        if list and len(list) == params.get('per_page'):
            page = params.get('page') + 1
            params.update([('page', page)])
            list.extend(self.__get_with_paginate(repo, link, params))
        return list
