from datetime import datetime
from urllib.parse import urlparse


def replace_tz(date: str) -> str:
    return date.replace('T', ' ').replace('Z', '')


def date_converter(date: str, format='%Y-%m-%d %H:%M:%S'):
    try:
        if not date:
            return
        return datetime.strptime(date, format)
    except ValueError:
        raise ValueError("Incorrect data format, should be {}".format(format))


def git_hub_url_parser(url):
    result = {}
    _name = ['user', 'repo']
    params = urlparse(url)
    if params.path:
        parts = params.path.split('/')[1:3]
        parts = list(filter(None, parts))
        for i in range(len(parts)):
            result.update([(_name[i], parts[i])])

    return result
