from collections import Counter
from datetime import datetime
from helpers.utils import replace_tz, date_converter


class Statistic:
    def __init__(self, date_from: datetime = None, date_till: datetime = None):
        self.__date_from = date_from
        self.__date_till = date_till

    @property
    def date_from(self):
        return self.__date_from

    @date_from.setter
    def date_from(self, val: datetime):
        self.__date_from = val

    @property
    def date_till(self):
        return self.__date_till

    @date_till.setter
    def date_till(self, val: datetime):
        self.__date_till = val

    def __check_date(self, date: str) -> bool:
        """
        :param date: current date
        :param _from: between from
        :param _till: between till
        :return: bool
        """

        is_check_from = True
        is_check_till = True

        if not date:
            return False

        _date = date_converter(date)

        if self.date_from and _date <= self.date_from:
            is_check_from = False

        if self.date_till and _date >= self.date_till:
            is_check_till = False

        return is_check_from and is_check_till

    def __get_param(self, item: dict, depth: list):
        node = item.copy()
        for param in depth:
            if node.get(param):
                node = node.get(param)
            else:
                return 'IS NULL {}'.format(param)
        return node

    def __counter(self, items, count_depth: list, date_depth: list) -> list:
        counter = Counter()

        for item in items:
            count_param = self.__get_param(item, count_depth)
            date = self.__get_param(item, date_depth)
            date = replace_tz(date)

            if self.__check_date(date) and count_param:
                counter[count_param] += 1

        return counter.most_common()

    def __get_older_items(self, items: list, days_older: int) -> int:
        date_now = datetime.now()
        count = 0
        for item in items:
            date = item.get('created_at')
            date = replace_tz(date)
            if self.__check_date(date):
                date = date_converter(date)
                diff = (date_now - date).days
                if diff > days_older:
                    count += 1
        return count


    def get_active_commit_users_stat(self, commits: list) -> list:
        """
        Не у всех юзеров есть логин
        https://github.community/t5/GitHub-API-Development-and/Request-for-commits-quot-author-quot-null-and-quot-committer/td-p/35343
        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """

        count_depth = ['author', 'login']
        date_depth = ['commit', 'committer', 'date']
        return self.__counter(commits, count_depth, date_depth)

    def get_pulls_stat(self, pulls: list) -> list:
        """
        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """
        count_depth = ['state']
        date_depth = ['created_at']
        return self.__counter(pulls, count_depth, date_depth)

    def get_issues_stat(self, issues: list) -> list:
        """
        :param commits: all commits from github
        :param _from: between from
        :param _till: between till
        :return: sorted list of tuple -> [(login, commits_count)]
        """
        count_depth = ['state']
        date_depth = ['created_at']
        return self.__counter(issues, count_depth, date_depth)

    def get_pulls_is_old_stat(self, pulls: list, days_older: int) -> int:
        return self.__get_older_items(pulls, days_older)

    def get_issues_is_old_stat(self, issues: list, days_older: int) -> int:
        return self.__get_older_items(issues, days_older)
