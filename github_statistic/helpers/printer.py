

class Printer:

    @staticmethod
    def print_scalar(val, text, header=None):
        if header:
            Printer.print_table_name(header)
        print(text + ' {}'.format(val))

    @staticmethod
    def print_table(colls: list, table_header: list = None, count=0, header=None):
        """
        :param items:
        :param header: columns name
        :param table_name: header
        :return: stdout table
        """
        if header:
            Printer.print_table_name(header)

        if not colls:
            print('{} Not found'.format(header))
            return

        if count:
            colls = colls[:count]

        formatted_string = ''
        padding = 3
        max_len = max(len(item) for item in table_header)
        # минимальный отсуп должен быть >= 10
        max_len = max([max_len, 10])

        for _ in table_header:
            formatted_string += '{:^' + str(max_len + padding*2) + 's}'

        print('-' * (max_len * padding))
        print(formatted_string.format(*table_header))
        print('-' * (max_len * padding))

        for col in colls:
            print(formatted_string.format(*list(map(lambda x: str(x), col))))

    @staticmethod
    def print_table_name(header):
        print()
        print()
        print('*' * 40)
        print('Статистика по {}'.format(header))
        print('*' * 40)