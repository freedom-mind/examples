import argparse

from datetime import datetime
from helpers.github_api import GitHubApi
from helpers.printer import Printer
from helpers.statistic import Statistic
from helpers.utils import git_hub_url_parser, date_converter


class Piplene:
    def __init__(self, user, repo, branch='master', date_from: datetime = None, date_till: datetime = None):
        self._from = date_from
        self._till = date_till
        self.user = user
        self.repo = repo
        self.branch = branch
        self.git_api = GitHubApi(user)
        self.stat = Statistic(date_from, date_till)

    def active_users_commit_stat(self):
        '''
        Самые активные участники.
        '''
        commits = self.git_api.get_commits(self.repo, self.branch)
        active_users = self.stat.get_active_commit_users_stat(commits)
        headers = ['Login', 'Commits count']
        Printer.print_table(active_users, table_header=headers, count=30, header="Active Commiters")

    def pulls_stat(self):
        '''
        Количество открытых и закрытых pull requests на заданном периоде времени
        '''
        pulls = self.git_api.get_pulls(self.repo, base_branch=self.branch, state='all')
        pulls_stat = self.stat.get_pulls_stat(pulls)

        headers = ['State', 'Count']
        Printer.print_table(pulls_stat, table_header=headers, header="PR")

    def pulls_old_stat(self):
        '''
        Количество “старых” pull requests на заданном периоде времени
        '''
        pulls_open = self.git_api.get_pulls(self.repo, base_branch=self.branch)
        pulls_old_stat = self.stat.get_pulls_is_old_stat(pulls=pulls_open, days_older=30)
        Printer.print_scalar(pulls_old_stat, 'Старых PR', header="PR Old")

    def issues_stat(self):
        '''
        Количество открытых и закрытых issues на заданном периоде времени по дате создания issue
        '''
        issues_open = self.git_api.get_issues(self.repo)
        issues_stat = self.stat.get_issues_stat(issues_open)
        headers = ['State', 'Count']
        Printer.print_table(issues_stat, table_header=headers, header="Issues")


    def issues_old_stat(self):
        '''
        Количество “старых” issues на заданном периоде времени по дате создания issue.
        '''
        issues_open = self.git_api.get_issues(self.repo)
        issues_old_stat = self.stat.get_issues_is_old_stat(issues=issues_open, days_older=14)
        Printer.print_scalar(issues_old_stat, 'Старых issues', header="Issues Old")

    def run(self):
        print('Началась загрузка данных, это может занять некоторео время...')
        self.active_users_commit_stat()
        self.pulls_stat()
        self.pulls_old_stat()
        self.issues_stat()
        self.issues_old_stat()


if __name__ == '__main__':
    DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
    try:
        parser = argparse.ArgumentParser(description='Приложение статистики github.com')
        parser.add_argument('-url', required=True, help='URL публичного репозитория на github.com')
        parser.add_argument('-b', '--branch', default='master', help='Ветка репозитория. По умолчанию - master.')
        parser.add_argument('-df', '--date_from',
                            help='Дата начала анализа. Если пустая, то неограничено. Формат: Y-m-d H:M:S')
        parser.add_argument('-dt', '--date_till',
                            help='Дата окончания анализа. Если пустая, то неограничено. Формат: Y-m-d H:M:S')

        args = parser.parse_args()

        url = git_hub_url_parser(args.url)

        date_from = date_converter(args.date_from, format=DATE_FORMAT)
        date_till = date_converter(args.date_till, format=DATE_FORMAT)

        if date_from and date_till:
            if date_from > date_till:
                raise Exception('date_from must be less then date_till')

        pipline = Piplene(user=url.get('user'),
                          repo=url.get('repo'),
                          branch=args.branch,
                          date_from=date_from,
                          date_till=date_till
                          )
        pipline.run()
    except Exception as e:
        print("Error: {}".format(e))
