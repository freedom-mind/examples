<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ""));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];


$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME"=>GetMessage("CP_BMS_IBLOCK_ID"),
			"TYPE"=>"LIST",
			"VALUES"=>$arIBlocks,
			"DEFAULT"=>'1',
			"MULTIPLE"=>"N",
			"ADDITIONAL_VALUES"=>"N",
			"REFRESH" => "Y",
		),
		"DEPTH_LEVEL" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CP_BMS_DEPTH_LEVEL"),
			"VALUES"=> Array(2=>2, 3=>3),
			"TYPE" => "LIST",
			"DEFAULT" => "2",
		),
		"HIT_CHECK" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CP_BMS_HIT_CHECK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => false,
			"REFRESH" => "Y",
		),
	),
);

?>
