<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function menuTree($items, $pid, $dLevel = 0)
{
    foreach ($items as $arItem)
    {
		if($arItem['SECTION_ID'] ==  NULL){
			$arItem['SECTION_ID'] = 0;
		}
        if ($arItem['SECTION_ID'] == $pid)
        {
			$treUL = menuTree($items, $arItem['ID'], $arItem['DEPTH_LEVEL']);
			
			if(!empty($treUL))
			{ 
				$parentArrow = ' <i class="fa fa-angle-down"></i>';
				$parent = ' parent';
			}
			
            $menuHtml .= '<li class="menu-chld-'.$dLevel.$parent.'"><a href="'.$arItem["SECTION_PAGE_URL"].'">' . $arItem['NAME'] . $parentArrow.'</a>';
			
			if($arItem['IMG']['src'] && $dLevel > 0)
			{ 
				$menuHtml .=  '<a class="popular-item" href="'.$arItem['IMGURL'].'"><img src="'.$arItem['IMG']['src'].'"></a>';
			}
            $menuHtml .=  $treUL;
            $menuHtml .= '</li>';
			$parentArrow  = '';
        }		
    }

    return $menuHtml ? '<ul class="menu-'.$dLevel.'">' . $menuHtml . '</ul>' : '';
}

echo menuTree($arResult["SECTIONS"], 0);

?>

