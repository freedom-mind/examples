<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if($arParams["DEPTH_LEVEL"]<=0)
	$arParams["DEPTH_LEVEL"]=1;

$arResult["SECTIONS"] = array();


if($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
	}
	else
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
			"GLOBAL_ACTIVE"=>"Y",
			"IBLOCK_ACTIVE"=>"Y",
			"<=DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
		);
		$arOrder = array(
	
		);

		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
			"ID",
			"DEPTH_LEVEL",
			"NAME",
			"SECTION_PAGE_URL",
			"IBLOCK_SECTION_ID",
		));

		while($arSection = $rsSections->GetNext())
		{
			$arImgFile = array();
			$imgUrl = '#';

			if($arParams["HIT_CHECK"] == "Y" )
			{
				$arFilter = array(
					"IBLOCK_ID"=>$arParams["IBLOCK_ID"], 
					"SECTION_ID"=>$arSection["ID"],
				);
				$arOrder = array(
					"show_counter"=>"DESC",
				);
				$res = CIBlockElement::GetList($arOrder, $arFilter, false, array('nTopCount'=>1), array("ID", "NAME", "SHOW_COUNTER", "DETAIL_PAGE_URL", "DETAIL_PICTURE"));
				while($ar_fields = $res->GetNext())
				{
					$arImgFile = CFile::ResizeImageGet(
						$ar_fields['DETAIL_PICTURE'],
						array("width" => 50, "height" => 50),
						BX_RESIZE_IMAGE_PROPORTIONAL,
						true, array()
					);
					$imgUrl = $ar_fields['DETAIL_PAGE_URL'];
				}
			}

			$arResult["SECTIONS"][] = array(
				"ID" => $arSection["ID"],
				"SECTION_ID" => $arSection["IBLOCK_SECTION_ID"],
				"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
				"NAME" => $arSection["~NAME"],
				"SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
				"IMG" => $arImgFile,
				"IMGURL" => $imgUrl,
			);
		}
		
		$this->includeComponentTemplate();
	}
}

?>
