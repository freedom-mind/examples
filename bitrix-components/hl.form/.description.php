<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_IBLOCK_COMMENT"),
	"DESCRIPTION" => GetMessage("T_IBLOCK_COMMENT_DESC"),
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "navigation",
			"NAME" => GetMessage("MAIN_COMMENT_SERVICE")
		)
	),
);

?>