<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="comments-component">

<? foreach($arResult["COMMENTS"] as $comment) {?>
<div class="single-comment">
<p class="comment-time"><?=$comment["UF_DATE"]->toString()?></p>
<p><b>Имя: </b><?=$comment["UF_NAME"]?><br> <b>E-mail: </b><?=$comment["UF_EMAIL"]?></p>

<p><b>Сообщение: </b><?=$comment["UF_COMMENT"]?></p>

</div>
<? } ?>
<hr>

<div class="comments">
	<form method="post" action="<?=$GLOBALS['APPLICATION']->GetCurPageParam()?>">
			<table class="customForm">
				<tbody>
					<tr>
						<td>
							<p><b>Имя</b> <span class="form-required">*</span></p>
						</td>
						<td>
							<input type="text" size="0" value="" placeholder="" name="REV_NAME" class="inptxt">
						</td>
					</tr>
					<tr>
						<td>
							<p><b>E-mail</b> <span class="form-required">*</span></p>
						</td>
						<td><input type="text" size="0" value="" placeholder="" name="REV_EMAIL" class="inptxt"></td>
					</tr>
					<tr>
						<td>
							<b>Комментарий</b> <span class="form-required">*</span>
						</td>
						<td>
							<textarea class="inptxt" rows="5" cols="40" name="REV_COMMENT"></textarea>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="2">
						<a class="sendBtn" onclick="comSend();">Отправить</a>
						
						<br>
							<p class="req"><span class="form-required">*</span> - обязательные поля </p>
						</th>
					</tr>
				</tfoot>
			</table>
			<input type="hidden" name="REV_URL" value="<?=$GLOBALS['APPLICATION']->GetCurUri()?>"/>
	</form>
	<div class="tnx">Спасибо, Ваше сообщение отправлено! Обновите страницу, чтобы увидеть его.</div>
</div>
</div>
