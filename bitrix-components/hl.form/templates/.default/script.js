function clearForm() {
	$('.comments .inptxt').each(function(){
		$(this).val('');
	});
}

function comSend(){
	var check = true;
	
	$('.comments .inptxt').each(function(){
		if($(this).val() == "") {
			$(this).addClass('error');
			check = false;
		}
	});
	
	if(check) {
		jQuery.ajax(
		{
			type: 'POST',
			url: window.location.href,
			data: jQuery(".comments form").serialize(),
			success: function(response)
			{
				jQuery(".tnx").css({'display':'block'});
				clearForm();
				
			}
		});
	}
	return false;
}

$(function() {
	$('.inptxt').on("change keyup input click", function() {
		jQuery(".tnx").css({'display':'none'});
		if($(this).hasClass('error')){
			$(this).removeClass('error');
		}
	});
	
});
 