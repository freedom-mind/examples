<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CJSCore::Init(array('jquery'));

$arFormFieldPrefix = "REV_";
$arFields = array(
	"URL"=>array("URL", "string"), 
	"NAME"=>array("ФИО", "string"), 
	"EMAIL"=>array("E-mail","string"), 
	"COMMENT"=>array("Комментарий","string"), 
	"DATE"=>array("Дата","datetime")
);

if($this->StartResultCache())
{
	if(!CModule::IncludeModule("highloadblock"))
	{
		$this->AbortResultCache();
	}
	else
	{
		$arHLParams = array();

		$hlGet = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('NAME'=>'Comments')));
		
		if ( !($hlBlock = $hlGet->fetch()))
		{
			$result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
				'NAME' => 'Comments',
				'TABLE_NAME' => 'comments',
			));
			
			if (!$result->isSuccess()) 
			{
				$errors = $result->getErrorMessages();
			} 
			else 
			{
				$ID = $result->getId();
				$oUserTypeEntity    = new CUserTypeEntity();
				foreach($arFields as $name => $type){
					$aUserFields    = array(
						'ENTITY_ID'         => 'HLBLOCK_'.$ID,
						'FIELD_NAME'        => 'UF_'.$name,
						'USER_TYPE_ID'      => $type[1],
						'MULTIPLE'          => 'N',
						'MANDATORY'         => 'N',
						'SHOW_FILTER'       => 'I',
						'EDIT_FORM_LABEL'   => array(
							'ru'    => $type[0],
							'en'    => $name,
						),
						'LIST_COLUMN_LABEL' => array(
							'ru'    => $type[0],
							'en'    => $name,
						),
					);
					 $iUserFieldId[] = $oUserTypeEntity->Add( $aUserFields );
				}
				$arHLParams["ID"] = $ID;
			}
		} 
		else 
		{
			$arHLParams["ID"] = $hlBlock["ID"];
		}
		

		$arHLBlock  = Bitrix\Highloadblock\HighloadBlockTable::getById($arHLParams["ID"])->fetch();

		$obEntity  = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
		$strEntityDataClass = $obEntity ->getDataClass();
		

		$arResult["COMMENTS"] = array();

		$Query = new \Bitrix\Main\Entity\Query($obEntity); 

		$Query->setSelect(array('*'));
		$Query->setFilter(array('UF_URL'=> array($GLOBALS['APPLICATION']->GetCurUri())));
		$Query->setOrder(array('UF_DATE' => 'DESC'));
		
		$result = $Query->exec();

		$result = new CDBResult($result);

		while ($row = $result->Fetch()){
			$arResult["COMMENTS"][] = $row;
		}

		if(isset($_REQUEST["REV_COMMENT"]))
		{
		
			 $APPLICATION->RestartBuffer();
			foreach($arFields as $field => $val)
			{
				if(isset($_REQUEST[$arFormFieldPrefix.$field]))
				{
					$arElementFields["UF_".$field] = htmlspecialchars($_REQUEST[$arFormFieldPrefix.$field]);
				}
			}
			
			$datetime =  new \Bitrix\Main\Type\DateTime;
			$arElementFields["UF_DATE"] = $datetime;


			$obResult = $strEntityDataClass::add($arElementFields);
			$ID = $obResult->getID();
			$bSuccess = $obResult->isSuccess();

			  $this->IncludeComponentTemplate();
			  die();
			
		}
		$this->includeComponentTemplate();
	}
}

?>
