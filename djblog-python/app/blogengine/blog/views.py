from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import View
# Create your views here.
from .forms import *
from .models import *
from .utils import *


class TagDetail(ObjectDetailMixin, View):
    model = Tag
    template = 'blog/tag_detail.html'


class TagUpdate(ObjectUpdateMixin, View):
    model = Tag
    form_model = TagForm
    template = 'blog/tag_update.html'


class TagDelete(ObjectDeleteMixin, View):
    model = Tag
    template = 'blog/tag_delete.html'
    redirect_url = 'tags_list_url'


class TagCreate(ObjectCreateMixin, View):
    form_model = TagForm
    template = 'blog/tag_create.html'


class PostDetail(ObjectDetailMixin, View):
    model = Post
    template = 'blog/post_detail.html'


class PostUpdate(ObjectUpdateMixin, View):
    model = Post
    form_model = PostForm
    template = 'blog/form/post/post_update.html'


class PostCreate(ObjectCreateMixin, View):
    form_model = PostForm
    template = 'blog/form/post/post_create.html'


class PostDelete(ObjectDeleteMixin, View):
    model = Post
    template = 'blog/form/post/post_delete.html'
    redirect_url = 'posts_list_urls'


def posts_list(request):
    posts = Post.objects.all()
    return render(request, 'blog/post_list.html', context={'posts': posts})


def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'blog/tags_list.html', context={'tags': tags})
