package com.hotel.ui.controller;

import com.hotel.backend.annotation.Autowired;
import com.hotel.ui.exception.MenuException;
import com.hotel.ui.menu.Menu;
import com.hotel.ui.model.builder.Builder;
import com.hotel.ui.model.builder.IBuilder;
import com.hotel.ui.model.navigator.INavigator;
import com.hotel.ui.model.navigator.Navigator;
import com.hotel.view.ViewController;

import java.util.Optional;
import java.util.Scanner;

public class MenuController {
    @Autowired(implementation = Builder.class)
    private IBuilder builder;
    @Autowired(implementation = Navigator.class)
    private INavigator navigator;


    public MenuController(IBuilder builder, INavigator navigator) {
        this.builder = builder;
        this.navigator = navigator;
    }

    public void run() {
        navigator.setCurrentMenu(builder.buildMenu());


        ViewController.getInstance().print("Welcome!");

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                navigator.printMenu();
                int index = scanner.nextInt();

                try {
                    navigator.navigate(index - 1);
                } catch (MenuException e) {
                    ViewController.getInstance().print("Invalid parameter? should be in range 1 " + navigator.getCurrentMenu().getItems().size());
                }

            }
        }
    }

}
