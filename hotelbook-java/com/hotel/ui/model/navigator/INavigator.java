package com.hotel.ui.model.navigator;

import com.hotel.ui.menu.Menu;

public interface INavigator {
    Menu getCurrentMenu();
    void setCurrentMenu(Menu currentMenu);
    void printMenu();

    void navigate(Integer index);
}
