package com.hotel.ui.model.navigator;

import com.hotel.ui.exception.MenuException;
import com.hotel.ui.menu.Menu;
import com.hotel.ui.menu.MenuItem;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.Optional;
import java.util.stream.Collectors;

public class Navigator implements INavigator {

    private Menu currentMenu;

    @Override
    public Menu getCurrentMenu() {
        return currentMenu;
    }

    @Override
    public void setCurrentMenu(Menu currentMenu) {
        this.currentMenu = currentMenu;
    }

    @Override
    public void printMenu() {
        ViewController.getInstance().print(currentMenu.getName() + " Chose item: ");
        ViewController.getInstance().printList(currentMenu.getItems().stream().map(MenuItem::getTitle).collect(Collectors.toList()));
    }

    @Override
    public void navigate(Integer index)  throws MenuException {

        if(index < 0 || index>=currentMenu.getItems().size()){
            throw new MenuException("Invalid index");
        }

        MenuItem subMenu = currentMenu.getItems().get(index);
        Optional.ofNullable(subMenu.getAction()).ifPresent(IAction::execute);

        currentMenu = subMenu.getNextMenu();

    }
}
