package com.hotel.ui.model.builder;

import com.hotel.ui.menu.Menu;
import com.hotel.ui.menu.MenuItem;
import com.hotel.ui.model.action.guest.AddGuestAction;
import com.hotel.ui.model.action.room.CheckInAction;
import com.hotel.ui.model.action.room.CheckOutAction;
import com.hotel.ui.model.action.guest.RemoveGuestAction;
import com.hotel.ui.model.action.room.AddRoomAction;
import com.hotel.ui.model.action.room.RemoveRoomAction;

import java.util.Arrays;

public class Builder implements IBuilder {

    private Menu rootMenu;

    public Builder(){
        rootMenu = new Menu("Root menu", null);
    }

    @Override
    public Menu buildMenu() {

        MenuItem addRoomMenu = new MenuItem("Add Room", rootMenu, new AddRoomAction());
        MenuItem removeRoomMenu = new MenuItem("Remove Room", rootMenu, new RemoveRoomAction());
        MenuItem addGuestMenu = new MenuItem("Add Guest", rootMenu, new AddGuestAction());
        MenuItem removeGuestMenu = new MenuItem("Remove Guest", rootMenu, new RemoveGuestAction());
        MenuItem checkInMenu = new MenuItem("Check In room", rootMenu, new CheckInAction());
        MenuItem checkOutMenu = new MenuItem("Check Out from room", rootMenu, new CheckOutAction());

        Menu roomMenu = new Menu("Rooms", Arrays.asList(addRoomMenu, removeRoomMenu, checkInMenu, checkOutMenu));
        Menu guestMenu = new Menu("Guest", Arrays.asList(addGuestMenu, removeGuestMenu));



        rootMenu.addItem(new MenuItem(roomMenu.getName(), roomMenu, null));
        rootMenu.addItem(new MenuItem(guestMenu.getName(), guestMenu, null));

        return rootMenu;
    }
}
