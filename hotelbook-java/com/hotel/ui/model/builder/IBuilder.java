package com.hotel.ui.model.builder;

import com.hotel.ui.menu.Menu;

public interface IBuilder {
    Menu buildMenu();
}
