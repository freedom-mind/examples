package com.hotel.ui.model.action.room;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;
import com.hotel.backend.exception.EntityNotFoundException;
import com.hotel.backend.service.IGuestService;
import com.hotel.backend.service.IRoomService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.List;
import java.util.Scanner;

public class CheckInAction implements IAction {
    @Override
    public void execute() {
        try {
            IRoomService roomService = Application.getInstance().getRoomService();
            IGuestService guestService = Application.getInstance().getGuestService();
            List<Room> rooms = roomService.getRooms();
            List<Guest> guests = guestService.getGuests();

            if (rooms.isEmpty() || guests.isEmpty()) {
                ViewController.getInstance().print("Rooms or Guests not found");
            } else {

                rooms.forEach(room ->
                        ViewController.getInstance().print("ID: " + room.getId() + ", Name: " + room.getName()));

                Scanner scanner = new Scanner(System.in);

                ViewController.getInstance().print("Please, input Room id to check in: ");
                int roomId = scanner.nextInt();

                guests.forEach(guest ->
                        ViewController.getInstance().print("ID: " + guest.getId() + ", Name: " + guest.getName()));

                ViewController.getInstance().print("Please, input Guest id to check in: ");
                int guestId = scanner.nextInt();

                roomService.checkIn(roomId, guestId);

                ViewController.getInstance().print("Guest check in");
            }
        } catch (EntityNotFoundException e) {
            ViewController.getInstance().print("ID not found: " + e.getId());
        }
    }
}
