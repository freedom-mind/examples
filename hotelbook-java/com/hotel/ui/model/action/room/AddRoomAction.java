package com.hotel.ui.model.action.room;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Room;
import com.hotel.backend.service.IRoomService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.Scanner;

public class AddRoomAction implements IAction {
    @Override
    public void execute() {
        ViewController.getInstance().print("Please, input room title");

        Scanner scanner = new Scanner(System.in);

        ViewController.getInstance().print("Name: ");
        String name = scanner.nextLine();

        ViewController.getInstance().print("Capacity: ");
        int capacity = scanner.nextInt();

        IRoomService roomService = Application.getInstance().getRoomService();
        Room room = roomService.save(new Room(name, capacity));

        ViewController.getInstance().print("Room created with id: " + room.getId());
    }
}
