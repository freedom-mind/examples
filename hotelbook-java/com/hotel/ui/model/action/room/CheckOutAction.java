package com.hotel.ui.model.action.room;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;
import com.hotel.backend.exception.EntityNotFoundException;
import com.hotel.backend.service.IGuestService;
import com.hotel.backend.service.IRoomService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.List;
import java.util.Scanner;

public class CheckOutAction implements IAction {
    @Override
    public void execute() {
        try {
            boolean is_empty = true;
            IRoomService roomService = Application.getInstance().getRoomService();
            IGuestService guestService = Application.getInstance().getGuestService();
            List<Room> rooms = roomService.getRooms();
            List<Guest> guests = guestService.getGuests();

            if (rooms.isEmpty() || guests.isEmpty()) {
                ViewController.getInstance().print("Rooms or Guests not found");
            } else {

                for (Room room : rooms) {
                    if (!room.getGuests().isEmpty()) {
                        is_empty = false;
                        ViewController.getInstance().print("Room ID: " + room.getId() + ", Name: " + room.getName());
                        ViewController.getInstance().print("Guests in this room");
                        room.getGuests().forEach(guest -> ViewController.getInstance().print("ID: " + guest.getId() + ", Name: " + guest.getName()));
                    }
                }

                if (!is_empty) {

                    Scanner scanner = new Scanner(System.in);

                    ViewController.getInstance().print("Please, input Room id to check out: ");
                    int roomId = scanner.nextInt();

                    ViewController.getInstance().print("Please, input Guest id to check out: ");
                    int guestId = scanner.nextInt();

                    roomService.checkOut(roomId, guestId);

                    ViewController.getInstance().print("Guest check out");
                }
            }
        } catch (EntityNotFoundException e) {
            ViewController.getInstance().print("ID not found: " + e.getId());
        }
    }
}
