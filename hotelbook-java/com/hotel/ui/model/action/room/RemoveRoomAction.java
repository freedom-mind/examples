package com.hotel.ui.model.action.room;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;
import com.hotel.backend.exception.EntityNotFoundException;
import com.hotel.backend.service.IGuestService;
import com.hotel.backend.service.IRoomService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.List;
import java.util.Scanner;

public class RemoveRoomAction implements IAction {
    @Override
    public void execute() {
        ViewController.getInstance().print("Please, input room to find: ");

        Scanner scanner = new Scanner(System.in);
        try {
            String name = scanner.nextLine();

            IRoomService roomService = Application.getInstance().getRoomService();
            List<Room> rooms = roomService.findByName(name);
            if (rooms.isEmpty()) {
                ViewController.getInstance().print("Nothing found");
            } else {
                rooms.forEach(room ->
                        ViewController.getInstance().print("ID: " + room.getId() + ", Name: " + room.getName()));

                ViewController.getInstance().print("Please, input id to delete: ");
                roomService.remove(scanner.nextInt());
            }
        } catch (EntityNotFoundException e) {
            ViewController.getInstance().print("Room not found: " + e.getId());
        }
    }
}
