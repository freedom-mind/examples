package com.hotel.ui.model.action;

public interface IAction {
    void execute();
}
