package com.hotel.ui.model.action.guest;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.exception.EntityNotFoundException;
import com.hotel.backend.service.IGuestService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.List;
import java.util.Scanner;

public class RemoveGuestAction implements IAction {

    @Override
    public void execute() {
        ViewController.getInstance().print("Please, input guest to find: ");

        Scanner scanner = new Scanner(System.in);
        try {
            String name = scanner.nextLine();

            IGuestService guestService = Application.getInstance().getGuestService();
            List<Guest> guests = guestService.findByName(name);
            if (guests.isEmpty()) {
                ViewController.getInstance().print("Nothing found");
            } else {
                guests.forEach(guest ->
                        ViewController.getInstance().print("ID: " + guest.getId() + ", Name: " + guest.getName()));

                ViewController.getInstance().print("Please, input id to delete: ");
                guestService.remove(scanner.nextInt());
            }
        } catch (EntityNotFoundException e) {
            ViewController.getInstance().print("Guest not found: " + e.getId());
        }
    }

}
