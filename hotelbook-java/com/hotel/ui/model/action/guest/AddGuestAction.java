package com.hotel.ui.model.action.guest;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.service.IGuestService;
import com.hotel.ui.model.action.IAction;
import com.hotel.view.ViewController;

import java.util.Scanner;

public class AddGuestAction implements IAction {
    @Override
    public void execute() {
        ViewController.getInstance().print("Please, input guest name");

        Scanner scanner = new Scanner(System.in);
        ViewController.getInstance().print("Name: ");
        String name = scanner.nextLine();

        IGuestService guestService = Application.getInstance().getGuestService();
        Guest guest = guestService.save(new Guest(name));

        ViewController.getInstance().print("Guest created with id: " + guest.getId());

    }
}
