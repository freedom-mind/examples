package com.hotel.ui.exception;

public class MenuException extends RuntimeException  {
    public MenuException(String message) {
        super(message);
    }
}
