package com.hotel.ui.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Menu {
    private String name;
    private List<MenuItem> items;

    public Menu(String name, List<MenuItem> items) {
        this.name = name;
        this.items = items;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }

    public void addItem(MenuItem item) {
        if(items == null) {
            items = new ArrayList<>();
        }
        Optional.ofNullable(items).ifPresent(menuItems -> menuItems.add(item));
    }
}
