package com.hotel.backend.service;

import com.hotel.backend.domain.Guest;

import java.util.Arrays;
import java.util.List;

public interface IGuestService {

    Guest save(Guest guest);

    List<Guest> findByName(String name);

    void remove(int id);

    Guest findById(int id);

    List<Guest> getGuests();
}
