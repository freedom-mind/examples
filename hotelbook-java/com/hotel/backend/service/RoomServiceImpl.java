package com.hotel.backend.service;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;
import com.hotel.backend.repository.RoomRepository;
import com.hotel.backend.repository.RoomRepositoryImpl;

import java.util.List;

public class RoomServiceImpl implements IRoomService {

    private RoomRepository roomRepository = new RoomRepositoryImpl();
    @Override
    public Room save(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public List<Room> findByName(String name) {
        return roomRepository.findByName(name);
    }

    @Override
    public void remove(int id) {
        roomRepository.remove(id);
    }

    @Override
    public List<Room> getRooms(){
        return roomRepository.getRooms();
    }

    @Override
    public Room findById(int id) {
        return roomRepository.findById(id);
    }

    @Override
    public void checkIn(int roomId, int guestId){
        Guest guest = Application.getInstance().getGuestService().findById(guestId);
        roomRepository.checkIn(roomId, guest);
    }

    @Override
    public void checkOut(int roomId, int guestId) {
        Guest guest = Application.getInstance().getGuestService().findById(guestId);
        roomRepository.checkOut(roomId, guest);
    }
}
