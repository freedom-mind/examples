package com.hotel.backend.service;

import com.hotel.backend.domain.Room;

import java.util.List;

public interface IRoomService {
    Room save(Room room);

    List<Room> findByName(String name);

    Room findById(int id);

    void remove(int id);

    List<Room> getRooms();

    void checkIn(int roomId, int guestId);

    void checkOut(int roomId, int guestId);
}
