package com.hotel.backend.service;

import com.hotel.backend.annotation.Autowired;
import com.hotel.backend.annotation.Component;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.repository.GuestRepository;
import com.hotel.backend.repository.GuestRepositoryImpl;

import java.util.List;

@Component
public class GuestServiceImpl implements IGuestService {

    @Autowired
    private GuestRepository guestRepository;

    GuestServiceImpl(){

    }

    @Override
    public Guest save(Guest guest) {
        return guestRepository.save(guest);
    }

    @Override
    public List<Guest> findByName(String name) {
        return guestRepository.findByName(name);
    }

    @Override
    public void remove(int id) {
        guestRepository.remove(id);
    }

    @Override
    public List<Guest> getGuests(){
        return guestRepository.getRooms();
    }

    @Override
    public Guest findById(int id) {
        return guestRepository.findById(id);
    }

}
