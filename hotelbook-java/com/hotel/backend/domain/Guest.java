package com.hotel.backend.domain;

import java.util.Random;

public class Guest {

    private String name;
    private Integer id ;

    public Guest(String name) {
        this.name = name;
        this.id = new Random().nextInt();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
