package com.hotel.backend.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class Room {

    private Integer id;
    private String name;
    private int roomCapacity;
    private RoomStatus status;
    private List<Guest> guests;

    public Room(String name, int capacity) {
        this.id = new Random().nextInt();
        this.name = name;
        this.roomCapacity = capacity;
        this.status = new RoomStatus();
        this.guests = new ArrayList<>();
    }

    public int getRoomCapacity() {
        return roomCapacity;
    }

    public int getRoomGuest() {
        return this.guests.size();
    }

    public void setRoomCapacity(int roomCapacity) {
        this.roomCapacity = roomCapacity;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoomStatus getStatus() {
        return status;
    }

    public void setStatus(RoomStatus status) {
        this.status = status;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuest(Guest guest) {
                
        if (Optional.ofNullable(guest).isPresent() && this.guests.size() < this.roomCapacity) {
            this.guests.add(guest);
            
            this.status.setBusy(this.guests.size() == this.roomCapacity);
        }
    }

    public void removeGuest(Guest guest) {

        if (Optional.ofNullable(guest).isPresent() && this.guests.contains(guest)) {
            this.guests.remove(guest);

            if (this.guests.size() == 0) {
                this.status.setBusy(false);
            }
        }
    }


    public Integer getId() {
        return id;
    }
}
