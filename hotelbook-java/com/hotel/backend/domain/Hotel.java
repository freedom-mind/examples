package com.hotel.backend.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Hotel {

    private String name;
    private List<Room> rooms;
    private List<Guest> guests;

    public Hotel(String name) {
        this.name = name;
        this.rooms = new ArrayList<>();
        this.guests = new ArrayList<>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void addRoom(Room room) {
        this.rooms.add(room);
    }

    public void addRooms(ArrayList<Room> rooms) {
        this.rooms.addAll(rooms);
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void addGuest(Guest guest) {
        this.guests.add(guest);
    }

    public void removeGuest(Guest guest) {
        this.guests.remove(guest);
    }

    public int getHotelCapacity() {
        return this.getRooms().stream().mapToInt(Room::getRoomCapacity).sum();
    }

    public int getChekInGuests() {
        return this.getRooms().stream().mapToInt(Room::getRoomGuest).sum();
    }

    public Room findRoom(String name) {
        return rooms.stream().filter(r -> r.getName().equals(name)).findFirst().orElse(null);
    }

    public Guest findGuest(String name) {
        return guests.stream().filter(r -> r.getName().equals(name)).findFirst().orElse(null);
    }

    public void checkInGuest(String roomName, List<Guest> guestsNames) {

        Room room = this.findRoom(roomName);
        boolean checkRoom = Optional.ofNullable(room).map(r->!r.getStatus().isBusy()&& !r.getStatus().isRepair()).orElse(false);
        if (checkRoom) {
            Optional.ofNullable(guestsNames).ifPresent(guests -> guests.forEach(g->Optional.ofNullable(this.findGuest(g.getName())).ifPresent(room::setGuest)));
        }

    }

    public void checkOutGuest(String roomName, List<Guest> guestsNames) {

        Room room = this.findRoom(roomName);

        if (Optional.ofNullable(room).isPresent()) {
            Optional.ofNullable(guestsNames).ifPresent(guests->guests.forEach(g->Optional.ofNullable(this.findGuest(g.getName())).ifPresent(room::removeGuest)));
        }

          //old version
//        if (Optional.ofNullable(room).isPresent() && Optional.ofNullable(guestsNames).isPresent()) {
//            guestsNames.stream().map(g -> this.findGuest(g.getName())).filter(Objects::nonNull).forEach(room::removeGuest);
//        }
    }

}

