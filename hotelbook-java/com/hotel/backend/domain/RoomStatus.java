package com.hotel.backend.domain;

public class RoomStatus {

    private boolean repair;
    private boolean busy;

    public RoomStatus(){
        repair = false;
        busy = false;
    }

    public boolean isRepair() {
        return repair;
    }

    public void setRepair(boolean repair) {
        this.repair = repair;
    }


    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

}
