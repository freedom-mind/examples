package com.hotel.backend.domain;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CommonIO {


    public List<String[]> csvReader(String filePath, String separator) {
        List<String[]> data = new ArrayList<>();
        List<String> fileLines = this.fileReader(filePath);
        for (String line : fileLines) {
            data.add(line.trim().split(separator));
        }

        return data;
    }

    public List<String> fileReader(String filePath) {
        List<String> data = new ArrayList<>();
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    public void fileWriter(String filePath, String data) {
        this.fileWriter(filePath, data, false);
    }

    public void fileWriter(String filePath, String data, boolean append) {
        File file = new File(filePath);
        OutputStream fileStream = null;
        try {
            fileStream = new FileOutputStream(file, append);
            fileStream.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileStream != null) {
                    fileStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
