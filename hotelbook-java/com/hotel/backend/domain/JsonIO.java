package com.hotel.backend.domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonIO {

    public JsonIO(){

    }

    public <T> List<T> getFromJson(String jsonPath) {

        Type type = new TypeToken<ArrayList<T>>(){}.getType();

        try (BufferedReader reader = new BufferedReader(new FileReader(jsonPath))) {

            return new Gson().fromJson(reader, type);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public <T> void saveToJson(T item) {
        List<T> items = new ArrayList<>();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        items = this.<T>getFromJson("rfefre");
        items.add(item);

        gson.toJson(items);
    }
}
