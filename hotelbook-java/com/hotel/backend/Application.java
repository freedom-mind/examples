package com.hotel.backend;

import com.hotel.backend.annotation.Autowired;
import com.hotel.backend.service.GuestServiceImpl;
import com.hotel.backend.service.IGuestService;
import com.hotel.backend.service.IRoomService;
import com.hotel.backend.service.RoomServiceImpl;

public class Application {

    private static Application instance;


    private IGuestService guestService;

    private IRoomService roomService;

    public static Application getInstance(){
        if (instance==null){
            instance = new Application();
        }

        return instance;
    }

    private Application(){
        this.guestService = new GuestServiceImpl();
        this.roomService = new RoomServiceImpl();
    }

    public IGuestService getGuestService() {
        return guestService;
    }

    public IRoomService getRoomService(){
        return roomService;
    }



}

