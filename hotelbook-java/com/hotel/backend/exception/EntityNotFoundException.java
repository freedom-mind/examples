package com.hotel.backend.exception;

public class EntityNotFoundException extends RuntimeException {
    private Integer id;

    public EntityNotFoundException(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
