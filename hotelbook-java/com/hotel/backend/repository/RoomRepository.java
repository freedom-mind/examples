package com.hotel.backend.repository;

import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;

import java.util.List;

public interface RoomRepository {
    Room save(Room room);

    void remove(int id);

    List<Room> findByName(String name);

    List<Room> getRooms();

    void checkIn(int roomId, Guest guest);

    Room findById(int id);

    void checkOut(int roomId, Guest guest);
}
