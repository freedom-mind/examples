package com.hotel.backend.repository;

import com.hotel.backend.annotation.Component;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.exception.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GuestRepositoryImpl implements GuestRepository {

    private List<Guest> guestList = new ArrayList<>();

    // Добавлять считывать из файла, удалить
    @Override
    public Guest save(Guest guest) {
        this.guestList.add(guest);
        return guest;
    }

    @Override
    public void remove(int id) {
        Guest toDelete = guestList.stream()
                .filter(g -> g.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(id));

        this.guestList.remove(toDelete);
    }

    @Override
    public List<Guest> findByName(String name) {
        return guestList.stream().filter(g -> name != null && g.getName()
                .toUpperCase()
                .startsWith(name.toUpperCase()))
                .collect(Collectors.toList());

    }

    @Override
    public Guest findById(int id) {
        return guestList.stream().filter(g -> g.getId().equals(id)).findFirst().orElseThrow(() -> new EntityNotFoundException(id));
    }

    @Override
    public List<Guest> getRooms() {
        return guestList;
    }
}
