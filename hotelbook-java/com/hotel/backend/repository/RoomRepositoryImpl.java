package com.hotel.backend.repository;

import com.hotel.backend.Application;
import com.hotel.backend.domain.Guest;
import com.hotel.backend.domain.Room;
import com.hotel.backend.exception.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RoomRepositoryImpl implements RoomRepository {
    private List<Room> roomList = new ArrayList<>();

    @Override
    public Room save(Room room) {
        this.roomList.add(room);
        return room;
    }

    @Override
    public void remove(int id) {
        Room toDelete = roomList.stream()
                .filter(g -> g.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(id));

        this.roomList.remove(toDelete);
    }

    @Override
    public List<Room> findByName(String name) {
        return roomList.stream().filter(g -> name != null && g.getName()
                .toUpperCase()
                .startsWith(name.toUpperCase()))
                .collect(Collectors.toList());
    }

    @Override
    public Room findById(int id) {
        return roomList.stream().filter(g -> g.getId().equals(id)).findFirst().orElseThrow(() -> new EntityNotFoundException(id));
    }

    @Override
    public List<Room> getRooms(){
        return roomList;
    }

    @Override
    public void checkIn(int roomId, Guest guest) {
        Room room = findById(roomId);
        room.setGuest(guest);
    }

    @Override
    public void checkOut(int roomId, Guest guest) {
        Room room = findById(roomId);
        room.removeGuest(guest);
    }

}
