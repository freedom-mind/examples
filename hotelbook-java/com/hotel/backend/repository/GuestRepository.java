package com.hotel.backend.repository;

import com.hotel.backend.domain.Guest;

import java.util.List;

public interface GuestRepository {
    Guest save(Guest guest);

    void remove(int id);

    List<Guest> findByName(String name);

    Guest findById(int id);

    List<Guest> getRooms();
}
