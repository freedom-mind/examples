package com.hotel.backend;

import com.hotel.backend.annotation.Autowired;
import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import java.lang.reflect.Field;
import java.util.*;

public class DI {

    private static Reflections reflections;
    private Map<Class<?>, Object> registry = new HashMap<>();

    public static <T> T init(T obj) {

        reflections = new Reflections("com.hotel",
                new FieldAnnotationsScanner(),
                new SubTypesScanner(),
                new TypeAnnotationsScanner());

        Field[] fields = obj.getClass().getDeclaredFields();
        for(Field field: fields) {
            Autowired autowired = field.getAnnotation(Autowired.class);
            if(autowired != null) {
                boolean accessible = field.isAccessible();
                field.setAccessible(true);
                try {
                    field.set(field.get(obj), autowired.implementation().newInstance());
                } catch (IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
                field.setAccessible(accessible);
            }
        }
        return obj;

    }

}



