package com.hotel;

import com.hotel.backend.DI;
import com.hotel.backend.annotation.Autowired;
import com.hotel.backend.annotation.Component;
import com.hotel.ui.model.builder.Builder;
import com.hotel.ui.controller.MenuController;
import com.hotel.ui.model.navigator.Navigator;
import org.reflections.Reflections;

import java.util.Set;


public class Main {

    public static void main(String[] args) {

//        MenuController controller= new MenuController(new Builder(), new Navigator());
        MenuController controller= DI.init(new MenuController());
        controller.run();
    }
}
