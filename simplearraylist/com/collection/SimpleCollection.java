package com.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.Arrays;

public class SimpleCollection<T> implements Collection {

    private T[] array;
    private int arrayCount;
    private final int ARRAY_SIZE = 2;
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE;


    public SimpleCollection() {
        this.array = (T[]) new Object[ARRAY_SIZE];
        this.arrayCount = 0;
    }

    @Override
    public int size() {
        return arrayCount;
    }

    @Override
    public boolean isEmpty() {
        return arrayCount == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < arrayCount; i++) {
            if (this.array[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new SimpleIterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(this.array, arrayCount);
    }


    private int newArraySize() {
        int oldSize = this.array.length;
        int newSize = oldSize + (oldSize >> 1);

        if (newSize < MAX_ARRAY_SIZE) {
            return newSize;
        } else {
            return MAX_ARRAY_SIZE;
        }
    }

    private Object[] addArraySize() {
        int newSize = newArraySize();
        return this.array = Arrays.copyOf(this.array, newSize);
    }


    private void add(Object o, Object[] arr, int count) {

        if (count == arr.length) {
            arr = addArraySize();
        }

        arr[count] = o;
        arrayCount++;
    }

    @Override
    public boolean add(Object o) {
        add(o, this.array, this.arrayCount);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int i;
        for (i = 0; i < arrayCount; i++) {
            if (this.array[i].equals(o)) {
                break;
            }
        }

        if (i == arrayCount) {
            return false;
        } else {
            if (arrayCount - i >= 0) {
                System.arraycopy(this.array, i + 1, this.array, i, arrayCount - i);
            }
            arrayCount--;
            return true;
        }

    }

    @Override
    public boolean addAll(Collection c) {
        Object[] newArr = c.toArray();

        if (newArr.length == 0) {
            return false;
        }

        for (int i = 0; i < newArr.length; i++) {
            this.add(newArr[i]);
        }
        return true;
    }

    @Override
    public boolean removeIf(Predicate filter) {
        boolean removed = false;

        final Iterator each = iterator();
        while (each.hasNext()) {
            Object item = each.next();
            if (filter.test(item)) {
                this.remove(item);
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public void clear() {
        for (int i = 0; i < arrayCount; i++) {
            this.array[i] = null;
        }
        this.arrayCount = 0;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean result = false;

        for (int i = 0; i < arrayCount; i++) {

            if (!c.contains(this.array[i])) {
                result = this.remove(this.array[i]);
                i -= 1;
            }
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean result = false;

        for (int i = 0; i < arrayCount; i++) {

            if (c.contains(this.array[i])) {
                result = this.remove(this.array[i]);
                i -= 1;
            }
        }
        return result;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (a.length < arrayCount) {
            return Arrays.copyOf(this.array, arrayCount, a.getClass());
        }
        System.arraycopy(this.array, 0, a, 0, arrayCount);

        if (a.length > arrayCount) {
            a[arrayCount] = null;
        }
        return a;
    }

    private class SimpleIterator implements Iterator {
        private int position = 0;

        public boolean hasNext() {
            return position < arrayCount;
        }

        public T next() {
            if (this.hasNext())
                return array[position++];
            else
                return null;
        }

        @Override
        public void remove() {

        }
    }

}
