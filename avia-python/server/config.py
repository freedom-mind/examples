import yaml
from pathlib import Path

class Config:
    def __init__(self):
        pass
    @staticmethod
    def get_config(config_file=None):
        if config_file is None:
            dir_path = Path(__file__).resolve().parents[2]
            config_file = str(dir_path) + '/config/config.yaml'
        with open(config_file, 'r') as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)