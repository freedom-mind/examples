import os
from gino import Gino


db = Gino()

DB_ARGS = dict(
    host=os.getenv('DB_HOST', 'localhost'),
    port=os.getenv('DB_PORT', 5432),
    user=os.getenv('DB_USER', 'postgres'),
    password=os.getenv('DB_PASS', '777'),
    database=os.getenv('DB_NAME', 'avia'),
)
PG_URL = 'postgresql://{user}:{password}@{host}:{port}/{database}'.format(
    **DB_ARGS)


class DbConnection:
    @staticmethod
    async def connect():
        await db.set_bind(PG_URL)

    @staticmethod
    async def create():
        await db.gino.create_all()
        # await db.pop_bind().close()