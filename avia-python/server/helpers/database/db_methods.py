import json
from sqlalchemy.dialects.postgresql import insert, UUID
from helpers.aviasales.models import City


async def get_data(model, id=None):
    if id:
        return await model.query.where(model.id == id).gino.first()
    return await model.query.gino.all()

async def insert_data(model, data, index=None):
    """
    Добавить новые данные в БД
    :param model: Таблица в БД
    :param data: Данные для добавления в таблицу
    :return:
    """
    # print(model)
    data = json.loads(data)
    # Обработчик для массива
    if type(data) is not list:

        if data.get('data') is not None:
            for item in data.get('data'):
                await insert(model).values(**item).on_conflict_do_nothing().gino.scalar()
    else:
        # Обработчик для словаря
        for item in data:
            await insert(model).values(**item).on_conflict_do_nothing().gino.scalar()
