import json
from helpers.aviasales.models.city import City
from helpers.database.db_methods import insert_data, get_data
from helpers.aviasales.utils import avs_api

class IataService:

    @staticmethod
    async def get_from_api():
        return await avs_api.get_iata()

    @staticmethod
    async def get_from_db(id=None):
        data = await get_data(City, id)
        return data

    @staticmethod
    async def insert():
        data = await IataService.get_from_api()
        await insert_data(City, data)

