import json
import asyncio
import pandas as pd
from helpers.aviasales.models.popular_direction import PopularDirection
from helpers.aviasales.models.popular_for_query import PopularForQuery
from helpers.aviasales.models.popular_for_query_history import PopularForQueryHistory
from helpers.aviasales.models.city import City
from helpers.aviasales.models.airline import Airline
from helpers.database.db_methods import insert_data, get_data
from helpers.aviasales.utils import avs_api, today, model_to_json
import logging

log = logging.getLogger('popular_direcitons_service')


class PopularDirectionsService:
    @staticmethod
    async def get_from_api(code):
        '''
        Самые популярные направлния авиакомпаний от авиасейлс
        :param code:
        :return:
        '''
        return await avs_api.get_popular_airline_directions(code)

    @staticmethod
    async def get_from_db(id=None):
        data = await get_data(PopularDirection, id)
        return data

    @staticmethod
    async def insert():

        """
        Получить популярные направления по авиакомпаниям
        :return:
        """
        """
        TODO:
        Запилить историю популярных направлений. Ежемесячно обновлять.
        """

        # удалиить старые записи из таблицы popular_direction
        await PopularDirection.delete.where(PopularForQuery.manual_add == False).gino.status()
        log.info("Start method: insert_popular_directions")

        air_items = await Airline.query.where(Airline.fly_from_russia == '1').gino.all()
        log.info("Count air_items in db: %s", len(air_items))

        for air_item in air_items:
            new_data = []
            code = air_item.code
            log.info("Запрос для направления [%s]", code)
            data = await PopularDirectionsService.get_from_api(code)
            data = json.loads(data)
            if data.get('data') is not None:
                for item in data.get('data'):
                    # print(item)
                    direction_names = item.split('-')
                    new_item = {
                        'direction_name': item,
                        'count': data['data'][item],
                        'airline_code': code,
                        'origin': direction_names[0],
                        'destination': direction_names[1],
                        'request_date': today
                    }

                    new_data.append(new_item)
            new_data = json.dumps(new_data)
            await insert_data(PopularDirection, new_data)
            log.info("Популярные направления для [%s] - добавлены", code)
            await asyncio.sleep(5)

    @staticmethod
    async def get_most_popular_directions():
        """
        Самые популярные направлния из БД
        :return:
        """
        pupular = await PopularForQuery.query.where(PopularForQuery.disable == False).gino.all()
        pupular = model_to_json(pupular)
        return pupular

    @staticmethod
    async def insert_most_popular_directions_for_query():
        """
        Выбрать самые популярные направления на основе таблицы popular_directions
        :return:
        """
        # удаляем предыдущие записи
        await PopularForQuery.delete.where(PopularForQuery.manual_add == False).gino.status()
        popular = await PopularDirection.query.gino.all()
        popular = model_to_json(popular)
        df = pd.read_json(json.dumps(popular))
        df = df.drop('id', axis=1)
        origin_mask = df['origin'] == 'MOW'
        df = df[origin_mask]
        df_popular = df.groupby(['destination'], as_index=False).sum().sort_values('count', ascending=False)
        df_popular = df_popular[df_popular['count'] > 40000]

        result_json = df_popular.to_json(orient='records')

        # добавить поле с названием направления для наглядности
        popular = []
        for item in json.loads(result_json):
            city_name = await City.query.where(City.code == item.get('destination')).gino.first()
            item.update({'city_name': city_name.name})
            popular.append(item)
        popular = json.dumps(popular)

        # самые популярные за текущий период
        await insert_data(PopularForQuery, popular)

        # история всех самых популярных рейсов
        await insert_data(PopularForQueryHistory, popular)

        # export_csv = df.to_csv(r'air.csv', index=None, header=True)
        # print(df)
