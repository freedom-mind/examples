import json
import asyncio
import pandas as pd
from helpers.aviasales.services.popular_directions_service import PopularDirectionsService
from helpers.aviasales.models.late import Late
from helpers.database.db_methods import insert_data, get_data
from helpers.aviasales.utils import avs_api, get_len_data
import logging

log = logging.getLogger('popular_direcitons_service')


class LatesSearchService:

    @staticmethod
    async def get_from_api(destination=None, limit=1000, page=1):
        return await avs_api.get_lates(destination=destination, limit=limit, page=page)

    @staticmethod
    async def get_from_db(id=None):
        data = await get_data(Late, id)
        return data

    @staticmethod
    def combine_data_from_request(data):
        data_list = []
        if data.get('data') is not None:
            for item in data.get('data'):
                data_list.append(item)

        return data_list

    @staticmethod
    async def insert():
        '''
        Возвращает список цен, найденных пользователями за последние 48 часов по самым популярным направлениям
        :return:
        '''

        # Удалить последние добавленные данные
        await Late.delete.gino.status()

        log.info("Start method: insert_lates_ticket")
        popular_directions = await PopularDirectionsService.get_most_popular_directions()
        page_limit = 1000

        if popular_directions is not None:
            for item in popular_directions:
                page_num = 1
                items = []
                # первый раз всегда заходим в while
                len_data = 1000

                while len_data >= page_limit:
                    data = await LatesSearchService.get_from_api(destination=item['destination'], limit=page_limit,
                                                                     page=page_num)
                    data = json.loads(data)
                    items += LatesSearchService.combine_data_from_request(data)
                    len_data = get_len_data(data)

                    log.info("destination = [%s], page_num = [%s], len_data = [%s]", item['destination'], page_num, len_data)

                    if len_data >= page_limit:
                        page_num += 1

                    await asyncio.sleep(5)

                await insert_data(Late, json.dumps(items))

        # data = await self.av.get_special_lates()
        # await insert_data(Lates, data)
