import json
import asyncio

from helpers.aviasales.models.calendar_price import CalendarPrice
from helpers.database.db_methods import insert_data, get_data
from helpers.aviasales.utils import avs_api, today, model_to_json
from helpers.aviasales.services.popular_directions_service import PopularDirectionsService
import logging as log


class CalendarService:

    @staticmethod
    async def get_from_api(destination):
        return await avs_api.get_calendar(destination)

    @staticmethod
    async def get_from_db(id=None):
        data = await get_data(CalendarPrice, id)
        return data

    @staticmethod
    async def insert():
        """
        Можно обновлять раз в 2-3 дня
        :return:
        """

        log.info("Start method: insert_special_calendar")

        popular_directions = await PopularDirectionsService.get_most_popular_directions()

        if popular_directions is not None:
            for item in popular_directions:
                data = await CalendarService.get_from_api(destination=item['destination'])
                data = json.loads(data)
                items = CalendarService.special_calendar_prepare_data(data)

                await insert_data(CalendarPrice, json.dumps(items))
                await asyncio.sleep(5)

    @staticmethod
    def special_calendar_prepare_data(items):
        result = []
        if items is not None:
            for k, v in items.get('data').items():
                item_dict = {}
                item_dict.update(v)
                item_dict.update([('calendar_day', k)])
                item_dict.update([('request_date', today)])
                result.append(item_dict)
                # print(item_dict)

        return result