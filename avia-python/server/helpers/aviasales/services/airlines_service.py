from helpers.aviasales.models.airline import Airline
from helpers.database.db_methods import insert_data, get_data
from helpers.aviasales.utils import avs_api


class AirlinesService:
    @staticmethod
    async def get_from_api():
        return await avs_api.get_airlines()

    @staticmethod
    async def get_from_db(id=None):
        data = await get_data(Airline, id)
        return data

    @staticmethod
    async def insert():
        """
        Получает и добавляет авиакомпании
        :return:
        """
        data = await AirlinesService.get_from_api()
        await insert_data(Airline, data)
