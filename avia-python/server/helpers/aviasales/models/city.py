from helpers.aviasales.models.types_helper import TextPickleType
from helpers.database.connector import db

class City(db.Model):
    __tablename__ = 'city'

    id = db.Column(db.Integer, primary_key=True)
    time_zone = db.Column(db.String)
    name = db.Column(db.String)
    coordinates = db.Column(TextPickleType())
    code = db.Column(db.String)
    cases = db.Column(TextPickleType())
    name_translations = db.Column(TextPickleType())
    country_code = db.Column(db.String)