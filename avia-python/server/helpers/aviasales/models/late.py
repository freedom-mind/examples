from helpers.database.connector import db


class Late(db.Model):
    __tablename__ = 'late'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer)
    return_date = db.Column(db.String)
    depart_date = db.Column(db.String)
    trip_class = db.Column(db.Integer)
    number_of_changes = db.Column(db.Integer)
    show_to_affiliates = db.Column(db.Boolean)
    actual = db.Column(db.Boolean)
    origin = db.Column(db.String)
    gate = db.Column(db.String)
    found_at = db.Column(db.String)
    duration = db.Column(db.Integer)
    distance = db.Column(db.Integer)
    destination = db.Column(db.String)
