from helpers.database.connector import db


class PopularDirection(db.Model):
    __tablename__ = 'popular_direction'

    id = db.Column(db.Integer, primary_key=True)
    direction_name = db.Column(db.String)
    count = db.Column(db.Integer)
    airline_code = db.Column(db.String)
    request_date = db.Column(db.String)
    origin = db.Column(db.String)
    destination = db.Column(db.String)
