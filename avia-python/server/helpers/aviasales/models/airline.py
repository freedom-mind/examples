from helpers.aviasales.models.types_helper import TextPickleType
from helpers.database.connector import db


class Airline(db.Model):
    __tablename__ = 'airline'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    code = db.Column(db.String)
    name_translations = db.Column(TextPickleType())
    fly_from_russia = db.Column(db.String)
