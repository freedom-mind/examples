from helpers.database.connector import db


class CalendarPrice(db.Model):
    __tablename__ = 'calendar_price'

    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String)
    destination = db.Column(db.String)
    price = db.Column(db.Integer)
    transfers = db.Column(db.Integer)
    airline = db.Column(db.String)
    flight_number = db.Column(db.Integer)
    return_at = db.Column(db.String)
    departure_at = db.Column(db.String)
    expires_at = db.Column(db.String)
    calendar_day = db.Column(db.String)
    request_date = db.Column(db.String)
