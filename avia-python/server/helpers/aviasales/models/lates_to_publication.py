from datetime import datetime
from helpers.database.connector import db

class LatesToPublication(db.Model):
    __tablename__ = 'lates_to_publication'

    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String)
    destination = db.Column(db.String)
    value = db.Column(db.Float)
    number_of_changes = db.Column(db.Integer)

    # duration = db.Column(db.Integer)
    # distance = db.Column(db.Integer)
    return_date = db.Column(db.String)
    depart_date = db.Column(db.String)
    gate = db.Column(db.String)
    # show_to_affiliates = db.Column(db.Boolean)
    # actual = db.Column(db.Boolean)

    found_at = db.Column(db.String, unique=True)
    add_date_time = db.Column(db.String, default=datetime.now().strftime("%Y-%m-%d"))


