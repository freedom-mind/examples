from datetime import datetime
from helpers.database.connector import db

class PopularForQueryHistory(db.Model):
    __tablename__ = 'popular_for_query_history'

    id = db.Column(db.Integer, primary_key=True)
    destination = db.Column(db.String)
    count = db.Column(db.String)
    city_name = db.Column(db.String)
    manual_add = db.Column(db.Boolean)
    disable = db.Column(db.Boolean)
    date_time = db.Column(db.String, default=datetime.now().strftime("%Y-%m-%d"))


