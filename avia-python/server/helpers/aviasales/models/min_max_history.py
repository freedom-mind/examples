from datetime import datetime
from helpers.database.connector import db

class MinMaxHistory(db.Model):
    __tablename__ = 'min_max_history'

    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String)
    number_of_changes = db.Column(db.Integer)
    destination = db.Column(db.String)
    min_price = db.Column(db.Float)
    median_price = db.Column(db.Float)
    max_price = db.Column(db.Float)
    current_month_count = db.Column(db.Integer)
    month = db.Column(db.String)
    found_at = db.Column(db.String, unique=True)

    add_date_time = db.Column(db.String, default=datetime.now().strftime("%Y-%m-%d"))


