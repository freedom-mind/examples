import json
from helpers.aviasales.aviasales_api import AviasalesApi
from datetime import datetime
from handler.aviasales.handler_utils import get_service_data, update_service_data

avs_api = AviasalesApi()
today = datetime.today().strftime('%Y-%m-%d')


def route_mapper(request):
    url_param = request.match_info.get('method', None)
    method = None
    query = request.query

    if url_param:
        route = {
            'get': get_service_data,
            'update': update_service_data,
            'remove': 'get_remove'
        }
        method = route.get(url_param)

    return method, query


def model_to_json(data) -> list:
    """
    Преобразуем объект SQLAlchemy в dict
    используетя после SELECT
    :param data: Данные из БД
    :return:
    """
    list_json = []
    for item in data:
        dct = item.__dict__
        if dct.get('__values__') is not None:
            list_json.append(dct.get('__values__'))

    return list_json


def get_len_data(data):
    """
    Счтиает количество записей полученных от api
    :param data:
    :return:
    """
    if data.get('data') is not None:
        return len(data.get('data'))
    else:
        return 0

def df_to_json(df):
    json_items = df.apply(lambda x: x.to_json(), axis=1)

    json_list = []
    for item in json_items:
        json_list.append(json.loads(item))

    return json.dumps(json_list)