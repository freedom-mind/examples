import json
import logging as log
import pandas as pd

from aiohttp import web
from helpers.aviasales.models.late import Late

log.basicConfig(level=log.INFO, format='%(name)s - %(levelname)s - %(message)s')

log.getLogger('gino').setLevel(log.ERROR)


def pretty_json(data):
    print(json.dumps(data, indent=4))


class GetDf(web.View):

    def get_month_data(self, df):
        items = df.groupby(['destination'], as_index=False).min().sort_values('value', ascending=True)
        return items.to_json(orient='records')

    async def get(self):
        dict_items = []
        # air_items = await CalendarPrices.query.gino.all()
        air_items = await Late.query.gino.all()
        for item in air_items:
            # print(item.__dict__.get('__values__'))
            dict_items.append(item.__dict__.get('__values__'))

        json_items = json.dumps(dict_items)
        df = pd.read_json(json_items)

        data = self.get_month_data(df)
        df.to_csv(r'C:\projects\avia\lates.csv', index=None, header=True)
        return web.json_response({
            'data': data
        })
