import logging as log
from aiohttp import web
from helpers.aviasales.utils import route_mapper
from helpers.aviasales.services.popular_directions_service import PopularDirectionsService


class PopularDirectionsView(web.View):
    async def get(self):

        method, query = route_mapper(self.request)

        # .../avia/popular/get - получаем из базы
        # .../avia/popular/update - обновляем и вставляем в базу
        data = await method(PopularDirectionsService, query)

        # расчитываем самые популярные направления
        await PopularDirectionsService.insert_most_popular_directions_for_query()



        return web.json_response({
            'View': 'PopularView'
        })