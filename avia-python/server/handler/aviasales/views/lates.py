import logging as log
from aiohttp import web
from helpers.aviasales.utils import route_mapper
from helpers.aviasales.services.lates_search_service import LatesSearchService


class LatesSearchServiceView(web.View):
    async def get(self):
        #/avia/lates/update
        method, query = route_mapper(self.request)
        data = await method(LatesSearchService, query)

        print(data)

        return web.json_response({
            'View': 'LatesSearchServiceView'
        })