import logging as log
from aiohttp import web
from helpers.aviasales.services.calendar_service import CalendarService


class CalendarView(web.View):
    async def get(self):
        url_param = self.request.match_info.get('method', None)
        url_q = self.request.query

        return web.json_response({
            'View': 'CalendarView'
        })