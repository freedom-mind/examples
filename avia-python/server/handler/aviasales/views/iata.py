import json
import logging as log
from aiohttp import web
from helpers.aviasales.utils import route_mapper
from helpers.aviasales.services.iata_service import IataService


class IataView(web.View):
    async def get(self):
        # Выбираем метод в зависимости от параметра в url (get, update, remove)
        method, query = route_mapper(self.request)
        data = await method(IataService, query)
        print(data)

        return web.json_response({
            'View': 'IataView'
        })
