import logging as log
from aiohttp import web
from helpers.aviasales.services.airlines_service import AirlinesService
from helpers.aviasales.utils import route_mapper



class AirlinesView(web.View):

    async def get(self):
        # Выбираем метод в зависимости от параметра в url (get, update, remove)
        method, query = route_mapper(self.request)
        data = await method(AirlinesService, query)

        return web.json_response({
            'View': 'AirlinesView'
        })
