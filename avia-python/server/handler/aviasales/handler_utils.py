async def get_service_data(service, query=None):
    '''
    Получить данные
    :param service:
    :param query:
    :return:
    '''
    if query and query.get('id'):
        return await service.get_from_db(query.get('id'))

    return await service.get_from_db()


async def update_service_data(service, *args):
    '''
    Обновить данные
    :param service:
    :param query:
    :return:
    '''

    await service.insert()
