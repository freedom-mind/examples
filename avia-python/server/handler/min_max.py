import json
import logging as log
import pandas as pd
from helpers.database.db_methods import insert_data
from helpers.aviasales.utils import df_to_json
from aiohttp import web
from helpers.aviasales.models.late import Late
from helpers.aviasales.models.lates_to_publication import LatesToPublication
from helpers.aviasales.models.min_max_history import MinMaxHistory

log.basicConfig(level=log.INFO, format='%(name)s - %(levelname)s - %(message)s')

log.getLogger('gino').setLevel(log.ERROR)


def pretty_json(data):
    print(json.dumps(data, indent=4))


class MinMax(web.View):

    def get_month_data(self, df):
        items = df.groupby(['destination'], as_index=False).min().sort_values('value', ascending=True)
        return items.to_json(orient='records')

    def get_min_of_year_for_publication(self, df):

        # минимум за весь период
        items_min = df.loc[df.groupby([df['origin'], df['destination']])['value'].idxmin()]
        # print(items_min)
        # items_min = items_min[(items_min['min_price'] < items_min['median_price']) & (items_min['median_price'] < items_min['max_price'])]
        items_min.drop(['id', 'duration', 'trip_class', 'show_to_affiliates', 'actual', 'distance'], axis=1, inplace=True)
        items_min['found_at'] = items_min['found_at'].astype(str)

        return df_to_json(items_min)

    async def get(self):
        dict_items = []
        # air_items = await CalendarPrices.query.gino.all()
        air_items = await Late.query.gino.all()
        for item in air_items:
            # print(item.__dict__.get('__values__'))
            dict_items.append(item.__dict__.get('__values__'))

        json_items = json.dumps(dict_items)
        df = pd.read_json(json_items)

        # получаем минимум цены за год для всех направлений, для публикации
        min_df = self.get_min_of_year_for_publication(df)
        await insert_data(LatesToPublication, min_df)

        # добавляем месяц
        df['month'] = pd.to_datetime(df['depart_date'], format='%Y-%m-%d').dt.to_period('M')

        # количество запросов на конкретный месяц
        df['current_month_count'] = df.groupby([df['origin'], df['destination'], df['month']])['value'].transform('count')
        # минум по месяцам
        items_by_month_min = df.loc[df.groupby([df['origin'], df['destination'], df['month']])['value'].idxmin()]

        # максимум по месяцам
        items_by_month_max = df.loc[df.groupby([df['origin'], df['destination'], df['month']])['value'].idxmax()]

        # медиана по месяцам
        items_by_month_median = df.groupby([df['origin'], df['destination'], df['month']], as_index=False).apply(
            lambda m: int(m['value'].median())).reset_index()

        # новое название столбцов
        items_by_month_median = items_by_month_median.rename(columns={0: 'median_price'})
        items_by_month_min = items_by_month_min.rename(columns={'value': 'min_price'})
        items_by_month_max = items_by_month_max.rename(columns={'value': 'max_price'})

        # объединения минимума, максимума и медианы в одну таблицу
        result = items_by_month_min.merge(items_by_month_median, on=['origin', 'destination', 'month'])
        result = pd.merge(result, items_by_month_max[['origin', 'destination', 'month', 'max_price']],
                          on=['origin', 'destination', 'month'])

        result.drop(['id', 'duration', 'return_date', 'gate', 'trip_class', 'depart_date', 'show_to_affiliates', 'actual', 'distance'], axis=1, inplace=True)

        result['month'] = result['month'].astype(str)
        result['found_at'] = result['found_at'].astype(str)
        result = result[result['current_month_count'] > 100]
        # print(result[result['destination'] == 'PRG'])
        # json_items = result.apply(lambda x: x.to_json(), axis=1)
        #
        # json_list = []
        # for item in json_items:
        #     json_list.append(json.loads(item))

        await insert_data(MinMaxHistory, df_to_json(result))
        # data = self.get_month_data(df)
        # df.to_csv(r'C:\projects\avia\lates.csv', index=None, header=True)
        return web.json_response({
            # 'data': data
        })
