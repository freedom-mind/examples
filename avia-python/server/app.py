import asyncio
import aiohttp_cors
from aiohttp import web
from handler.data_manage import DataManage
from handler.getdf import GetDf
from handler.min_max import MinMax
from helpers.aviasales.models import *
from helpers.database.connector import DbConnection
from handler.aviasales.views import AirlinesView, IataView, LatesSearchServiceView, \
    CalendarView, PopularDirectionsView


async def init_app():
    await DbConnection.connect()
    await DbConnection.create()

    app = web.Application()
    cors = aiohttp_cors.setup(app)

    resource = cors.add(app.router.add_resource("/df"))
    route = cors.add(
        resource.add_route("GET", GetDf), {
            "http://localhost:3000": aiohttp_cors.ResourceOptions(
                allow_credentials=True
            )
        })
    app.router.add_view('/mm', MinMax)
    app.router.add_view('/data/{method}', DataManage)
    app.router.add_view('/avia/airlines/{method}', AirlinesView)
    app.router.add_view('/avia/iata/{method}', IataView)
    app.router.add_view('/avia/lates/{method}', LatesSearchServiceView)
    app.router.add_view('/avia/calendar/{method}', CalendarView)
    app.router.add_view('/avia/popular/{method}', PopularDirectionsView)
    # app.router.add_view('/df', GetDf)

    return app


def main():
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init_app())
    web.run_app(app, host="127.0.0.1", port=8080)


if __name__ == '__main__':
    main()
