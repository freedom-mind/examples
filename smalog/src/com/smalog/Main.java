package com.smalog;

public class Main {

    public static void main(String[] args) {
        FileReader fl = new FileReader();
        long startTime = System.nanoTime();

        String txt = fl.readFile("FileWriter.txt", 10);
        System.out.println(txt);

        long endTime = System.nanoTime();

        double duration = (double) (endTime - startTime)/1_000_000_000;
        System.out.println(duration + " seconds");


    }
}
