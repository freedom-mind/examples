package com.smalog;

import com.jcraft.jsch.*;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Connector {
    private String login;
    private String password;
    private String hostname;
    private int port;

    public Connector(String login, String password, String hostname, int port) {
        this.login = login;
        this.password = password;
        this.hostname = hostname;
        this.port = port;
    }

    public void connectionTest() {
        try {

            String remoteFile = "";

            JSch jsch = new JSch();
            Session session = jsch.getSession(this.login, this.hostname, this.port);
            session.setPassword(this.password);
            session.setConfig("StrictHostKeyChecking", "no");
            System.out.println("Establishing Connection...");
            session.connect();
            System.out.println("Connection established.");
            System.out.println("Crating SFTP Channel.");
            ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();
            System.out.println("SFTP Channel created.");

            InputStream inputStream = sftpChannel.get(remoteFile);

            try (Scanner scanner = new Scanner(new InputStreamReader(inputStream))) {
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                }
            }
        } catch (JSchException | SftpException e) {
            e.printStackTrace();
        }
    }


}
