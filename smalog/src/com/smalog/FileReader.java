package com.smalog;

import java.io.*;
import java.nio.Buffer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.collections4.queue.CircularFifoQueue;

public class FileReader {
    public String readFile(String filePath, int lineCount) {
        InputStream fileStream = null;
        CircularFifoQueue<String> buffer = new CircularFifoQueue<>(lineCount);
        try {
            fileStream = new FileInputStream(filePath);
            LineIterator lineIterator = org.apache.commons.io.IOUtils.lineIterator(fileStream,"UTF-8");
            String lastLine="";
            while (lineIterator.hasNext()){
                lastLine =  lineIterator.nextLine();
                buffer.add(lastLine);
            }
            System.out.println(lastLine);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileStream != null) {
                try {
                    fileStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        StringBuilder text = new StringBuilder();
        for (Object txt : buffer) {
            text.append(txt);
            text.append(System.getProperty("line.separator"));
        }
        return text.toString();

    }

    public void writeFite(String data) {
        OutputStream os = null;
        try {
            os = new FileOutputStream(new File("os.txt"));
            os.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static void writeFileWriter(String data, String filePath) {
        File file = new File(filePath);
        OutputStream fr = null;
        try {
            fr = new FileOutputStream(file, true);
            fr.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String TextGenerator(String filePath){

        String str = "Hi this is big text! Very very big log file ohhh! \n";
        StringBuilder text = new StringBuilder();

        text.append(str.repeat(10000000));
        writeFileWriter(text.toString(), filePath);

        return text.toString();
    }
}

