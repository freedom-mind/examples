$(function () {
    var EW = {
        config: {
            progressBar: $('.progress-bar'),
            progressBarCorrectCount: $('.progress-bar .correct-count'),
            progressCount: $('.progress-bar .all-count'),
            progressLine: $('.progress-bar .bar span'),
            wordEngWord: $('.words-block .eng-word'),
            testEngWord: $('.test-block .eng-word'),
            rusTranslated: $('.rus-translated'),
            showTranslate: $('.show-translate'),
            wordsBlock: $('.words-block'),
            testBlock: $('.test-block'),
            testList: $('.test-block .test-list'),
        },
        loadWords: {},
        showedWords: [],
        showedCount: 10,
        testWords: [], //словарь тестирования
        testWordCid: 0, //d слова в тестировании
        testLang: 'en', //язык тестирования
        testLangCount: 0, //язык тестирования
        testCorrectEnCount: 0, //язык тестирования
        testCorrectRuCount: 0, //язык тестирования

        getWordsFromDb: function (params = 0) {
            var words = '';
            var user = 1;

            if (typeof (params.len) == 'undefined' || params.len === null) {
                params = {'len': this.showedCount};
            }

            $.ajax({
                url: "en.php",
                type: 'POST',
                async: false,
                data: {param: params},
                success: function (data) {
                    words = JSON.parse(data);
                }
            });

            return words;
        },

        getRandomWord: function (obj) {
            var random = Math.floor(Math.random() * (obj.length - 0));
            //добавить чтобы слова не повторялись if(!showedWords)
            return obj[random];
        },

        setBrTag: function (str, lang) {

            if (lang === 'ru') return str;

            var re = /(\w+)\s(\w+)/;
            return str.replace(/\s(\W.*)/, '<span class="transcr">$1</span>');
        },
        removeTranscription: function (str) {

            return str.replace(/\s(\W.*)/, '');
        },

        letStart: function (obj) {
            //if(EW.showedWords.length < EW.showedCount) {

            var word = this.getRandomWord(obj);
            this.loadWords = this.removeElement(this.loadWords, word);
            this.showedWords.push(word);

            $(this.config.showTranslate).show();
            $(this.config.rusTranslated).hide();

            $(this.config.wordEngWord).html(this.setBrTag(word.en_word));
            $(this.config.rusTranslated).html(word.ru_word);
            //} else {


            //тестирование по словам из EW.showedWords

            //после тестирование очищаем массив и снова запрашиваем 10 слов исключая предеыдущие

            //}
        },
        showTestBlock: function () {
            $(this.config.wordsBlock).fadeOut(100);
            $(this.config.testBlock).fadeIn(100);
        },
        showWordBlock: function () {
            $(this.config.wordsBlock).fadeIn(100);
            $(this.config.testBlock).fadeOut(100);
        },

        testHtml: function (words, lang = 'en') {
            var html = '';
            for (i = 0; i < words.length; i++) {
                if (lang !== 'ru') {
                    html += '<span class="item" data-id="' + words[i].id + '">' + words[i].ru_word + '</span>';
                } else {
                    html += '<span class="item" data-id="' + words[i].id + '">' + this.removeTranscription(words[i].en_word) + '</span>';
                }
            }
            return html;
        },

        typeOfTestRuOrEn: function (type = 'en') {

            //рандомно выбираем слово из пройденых
            var wordForTest = this.getRandomWord(this.testWords);
            //разные слова
            var failWords;

            var params = {
                'currentWord': wordForTest.id, //исключаем текущее слово из запроса
                'len': 5 //запрашиваем 5 слов из базы
            };
            //удаляем текущее слово из пройденых слов, чтобы они не повторялись
            this.testWords = this.removeElement(this.testWords, wordForTest);

            //подгружаем неправильные переводы для тестирования
            failWords = this.getWordsFromDb(params);
            failWords.push(wordForTest); //добавляем текущее слово к массиву переводов

            failWords.sort(this.randomSort); //перемешиваем слова

            //id слова
            this.testWordCid = wordForTest.id;

            var langTranslate = (type === 'en') ? wordForTest.en_word : wordForTest.ru_word;

            //генерим html код для теста
            this.config.testEngWord.attr({'data-id': this.testWordCid});
            this.config.testList.html(this.testHtml(failWords, type));
            this.config.testEngWord.html(this.setBrTag(langTranslate, type))
        },

        progressClear: function () {
            this.config.progressLine.removeAttr('style');
            this.config.progressBarCorrectCount.html('0');
            this.testCorrectEnCount = this.testCorrectRuCount = 0;
        },
        progressBar: function () {
            var progress = (this.testLang === 'en') ? this.testCorrectEnCount : this.testCorrectRuCount;
            if (progress !== 0) {
                var progressPercent = (progress / this.showedCount) * 100;
                this.config.progressLine.attr({'style': 'width:' + progressPercent + '%'})
            }
            this.config.progressBarCorrectCount.html(progress);
            this.config.progressCount.html(this.showedCount);
        },

        letTestStart: function () {
            this.progressBar();

            //Создаем тест на основе пройденых слов
            //Если словарь тестирования пуст
            if (this.testWords.length === 0 && this.testLangCount < this.showedCount) {
                this.progressClear();
                this.testWords = this.showedWords;

                //открываем блок тестирования
                this.showTestBlock();
            }

            if (this.testWords.length > 0) {
                this.typeOfTestRuOrEn(this.testLang)
            }

            //console.log(failWords);

            if (this.testLang === 'ru') {
                this.testLangCount++;
            }
            console.log(this.testLangCount)
            //после тестирования загружаем новые n слов
            if (this.testWords.length == 0) {
                this.testLang = 'ru';
                //this.progressClear();
            }

            if (this.testLang === 'ru' && this.testLangCount > this.showedCount) {
                this.showedWords = [];
                this.testLangCount = 0;
                this.testLang = 'en';
                //добавить исключения показанных слов
                this.loadWords = this.getWordsFromDb();
                this.showWordBlock();
            }
        },

        compareTestVariant: function (word_id) {
            var result = false;
            if (word_id === this.testWordCid) {
                result = true
            }
            return result;
        },

        setTestVariant: function (obj) {
            if (this.compareTestVariant(obj.data('id'))) {
                obj.addClass('correctly');

                (this.testLang === 'en') ? this.testCorrectEnCount++ : this.testCorrectRuCount++;
                console.log(this.testCorrectEnCount);
                console.log(this.testCorrectRuCount);

            } else {
                obj.addClass('not-correctly')
            }
        },

        showTestOrWords: function (word_id) {
            if (this.showedWords.length < this.showedCount) {
                this.progressClear();
                this.letStart(this.loadWords);
            } else {
                this.letTestStart();
            }
        },

        /*
        * удаляет элмент из массива
        */
        removeElement: function (obj, element) {
            var arr = [];
            for (i = 0; i < obj.length; i++) {
                if (obj[i].id !== element.id) {
                    arr.push(obj[i]);
                }
            }
            return arr;
        },
        /*
        * перемешивает массив
        */
        randomSort: function (a, b) {
            return Math.random() - 0.5;
        }

    };
    Array.prototype.remove = function (value) {
        var idx = this.indexOf(value);
        if (idx !== -1) {
            // Второй параметр - число элементов, которые необходимо удалить
            return this.splice(idx, 1);
        }
        return false;
    }

//выбираем 10 слов
//учим их
//проходим тест по этим словам, сначала с английского на русский, затем наоборот
//слова которые не учить, отсылаем запрос к базе об этом
//запрашиваем новые 10 слов


    EW.loadWords = EW.getWordsFromDb();
    EW.letStart(EW.loadWords);


    $('.remember').on('click', function (event) {
        EW.showTestOrWords();
    });

    $('.show-translate').on('click', function () {
        $(this).hide();
        EW.config.rusTranslated.show();
    });

    $(document).on('click', '.test-list .item', function (event) {

        EW.setTestVariant($(this));
        setTimeout(function () {
            EW.showTestOrWords();
        }, 500);

    });
});